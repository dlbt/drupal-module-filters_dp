(function($) {
  Drupal.behaviors.filters_dp = {
    attach: function(context, settings) {
      if (Drupal.settings.filters_dp && Drupal.settings.filters_dp.fid) {
        switch(Drupal.settings.filters_dp.fid) {
          case "1":
            break;
          case "2":
            var color = "#f6f4b7";
            var hoverColor = '#faf9d7';
            break;
          case "3":
            var color = "#c9e1ea";
            var hoverColor = '#e2edf1';
            break;
          case "4":
            var color = "#b5cb88";
            var hoverColor = '#cddeab';
            break;
        }
        
        if (color) {
          var menu = $("#main-menu ul.menu li");
          menu.each(function( index ) {
            var link = $(this).find("a").attr("href");
            link = link.replace("/node/","");
            if ($.inArray(link, Drupal.settings.filters_dp.nodes) > -1) {
              $(this).css({"backgroundColor": color});
	      // Add hover color too
              $(this).hover(
                function() {
		  $(this).css({"backgroundColor": hoverColor});
		},
		function() {
		  $(this).css({"backgroundColor": color});
		}
	      );

              // Color also the parent menus
              $(this).parents("li").css({"backgroundColor": color});
              $(this).parents("li").hover(
                function() {
                  $(this).css({"backgroundColor": hoverColor});
                },
                function() {
                  $(this).css({"backgroundColor": color});
                }
              );
            }
          });
        }

        // Starting page
        var pathname = window.location.pathname;
        var node = pathname.replace("/node/", "");
        var pages = ["33", "34", "179"];
        if ($.inArray(node, pages) > -1) {
          url = "https://dutchplusplus.ned.univie.ac.at/sites/all/modules/custom/filters_dp/js/img" + pages[$.inArray(node, pages)] + ".png";
          $("h1.title").css({"background": "url(" + url + ") no-repeat"});
          $("h1.title").css({"height": "60px"});
          $("h1.title").css({"text-align": "center"});
          $("h1.title").css({"padding-top": "20px"});
          $("h1.title").css({"margin-left": "50px"});
        }
      }
    }
  }
})(jQuery);
