<?php

/**
 * @file - Diverse functions
 */
 
function _filters_dp_insert_record($nid, $fid) {
  $value = new stdClass();
  $value->nid = $nid;
  $value->fid = $fid;
  
  $key = _filters_dp_find_record($nid, $fid);
  
  drupal_write_record('filters_dp_content', $value, $key);
}

function _filters_dp_find_record($nid, $fid) {
  
  $key = array();
  
  $query = db_select('filters_dp_content', 'c')
    ->fields('c', array('id'))
    ->condition('nid', $nid)
    ->condition('fid', $fid)
    ->execute();
  
  $id = $query->fetchAssoc();
  
  if (!empty($id)) {
    $key = array('id');
  }
  
  return $key;
}

function _filters_dp_get_filters() {
  $query = db_select('filters_dp_list', 'l')
    ->fields('l')
    ->execute();
  
  $filters = $query->fetchAll();
    
  return $filters;
}

function _filters_dp_get_nodes() {
  
  $header = array(
    'Node ID',
    'Title',
    'Filter',
  );
  
  $rows = array();
  
  $results = db_select('filters_dp_content', 'c')
    ->fields('c')
    ->execute();
    
  foreach ($results as $value) {
    if ($value->nid != 0) {
      $node = node_load($value->nid);
      $filter = _filters_dp_load_filter($value->fid);
      $rows[] = array(
        $value->nid,
        $node->title,
        $filter->title,
      );
    }
  }
    
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function _filters_dp_load_filter($fid) {
  $query = db_select('filters_dp_list', 'l')
    ->fields('l')
    ->condition('id', $fid)
    ->execute();
    
  return $query->fetch();
}

function _filters_dp_admin_submit_handler(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/filters_dp';
}

function _filters_dp_activate_filter($fid) {
 
  if ($fid != 0) {
    $_SESSION['filters_dp']['nodes'] = _filters_dp_get_nodeID_by_fid($fid);
    if ((isset($_SESSION['filters_dp']['fid']) && $_SESSION['filters_dp']['fid'] != $fid) || !isset($_SESSION['filters_dp']['fid'])) {
    	$_SESSION['filters_dp']['fid'] = $fid;
    	$nid = get_filter_main_page($fid);
    	drupal_goto("node/" . $nid);
    }
    else {
      _filters_dp_remove_filter();
      drupal_goto('<front>');
    }
  }
  else {
    _filters_dp_remove_filter();
    drupal_goto('<front>');
  }
}

function _filters_dp_get_nodeID_by_fid($fid) {
  
  $query = db_select('filters_dp_content', 'c')
    ->fields('c')
    ->condition('fid', $fid)
    ->execute();
  
  $nids = array();
  
 foreach ($query as $result) {
    $nids[] = $result->nid;
  }
  
  return $nids;
}

function _filters_dp_remove_filter() {
  unset($_SESSION['filters_dp']['nodes']);
  $_SESSION['filters_dp']['fid'] = 1;
}

function get_filter_main_page($fid) {
  $query = db_select('filters_dp_list', 'c')
    ->fields('c', array('main_page'))
    ->condition('id', $fid)
    ->execute();

  $nid = $query->fetchField();

  return $nid;
}

function _filters_dp_list_insert_record($title, $main_page) {
  $value = new stdClass();
  $value->title = $title;
  $value->main_page = $main_page;

  drupal_write_record('filters_dp_list', $value, array());
}

function _filters_dp_list_filters() {

  $header = array(
    'Filter ID',
    'Title',
    'Main Page',
  );

  $rows = array();

  $results = db_select('filters_dp_list', 'c')
    ->fields('c')
    ->execute();

  foreach ($results as $value) {
    $node = node_load($value->main_page);
    $rows[] = array(
      $value->id,
      $value->title,
      $node->title,
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}
