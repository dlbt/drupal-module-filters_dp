<?php

/**
 * All functions that return HTML as output
 */
 
function _filters_dp_block_content() {

  global $base_url; 
 
  $html = '<div id="filters">';
  
  $filters = _filters_dp_get_filters();
  
 foreach ($filters as $filter) {
    $image = $base_url . '/' . drupal_get_path('module', 'filters_dp'). '/images/' . strtolower($filter->title) . '_off.jpg';
    if (isset($_SESSION['filters_dp']) && $_SESSION['filters_dp']['fid'] == $filter->id) {
	$image = $base_url . '/' . drupal_get_path('module', 'filters_dp'). '/images/' . strtolower($filter->title) . '_on.jpg';
    }
 
    $html .= '<div id="' . strtolower($filter->title) .'" class="filter-box">';
    $html .= '<a href="' . $base_url . '/set-filter/' . $filter->id . '"><img src="' . $image . '" /></a>';
    $html .= '</div>';
  }
  
  $html .= '</div>';
  
  return $html;
}
