<?php

/**
 * @file - All forms go in this file
 */


function filters_dp_settings_form($form, &$form_state) {
  $form = array();
  
  $form['list'] = array(
    '#markup' => _filters_dp_get_nodes(),
  );

  $filters = _filters_dp_get_filters();
  $options = array();
  foreach ($filters as $filter) {
    $options[$filter->id] = $filter->title;
  }
 
  $form['filter'] = array(
    '#type' => 'select',
    '#title' => t('Select filter'),
    '#options' => $options,
  );
  
  $form['node'] = array(
    '#type' => 'textfield',
    '#title' => t('Nid'),
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  
  return $form;
}

function filters_dp_settings_form_submit($form, &$form_state) {
  $nid = $form_state['values']['node'];
  $fid = $form_state['values']['filter'];
  
  _filters_dp_insert_record($nid, $fid);
  
  drupal_set_message(t('Node added to filter'));
}

function filters_dp_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['list'] = array(
    '#markup' => _filters_dp_list_filters(),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );

  $form['main_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Nid of the main page'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function filters_dp_admin_settings_form_submit($form, &$form_state) {
  $title = $form_state['values']['title'];
  $main_page = $form_state['values']['main_page'];

  _filters_dp_list_insert_record($title, $main_page);

  drupal_set_message(t('Node added'));
}
